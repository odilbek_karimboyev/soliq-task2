package com.example.excelimport.service;

import com.example.excelimport.payload.StudentRequestDto;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ExcelService {

    List<StudentRequestDto> readFile(MultipartFile file);
}
