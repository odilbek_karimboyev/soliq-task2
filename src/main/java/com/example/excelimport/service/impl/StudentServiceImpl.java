package com.example.excelimport.service.impl;

import com.example.excelimport.mapper.StudentMapper;
import com.example.excelimport.payload.StudentRequestDto;
import com.example.excelimport.repository.StudentRepository;
import com.example.excelimport.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final StudentMapper mapper;

    @Override
    public void create(List<StudentRequestDto> request) {
        studentRepository.saveAll(mapper.fromDtoList(request));
    }
}
