package com.example.excelimport.service.impl;

import com.example.excelimport.payload.StudentRequestDto;
import com.example.excelimport.service.ExcelService;
import com.example.excelimport.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class ExcelServiceImpl implements ExcelService {

    private final StudentService studentService;

    @Override
    public List<StudentRequestDto> readFile(MultipartFile file) {
        List<StudentRequestDto> studentList = new ArrayList<>();
        XSSFWorkbook workbook;
        try {
            workbook = new XSSFWorkbook(file.getInputStream());

            XSSFSheet worksheet = workbook.getSheetAt(0);

            for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
                StudentRequestDto student = new StudentRequestDto();

                XSSFRow row = worksheet.getRow(i);

                try {
                    student.setFirstName((row.getCell(0).getStringCellValue()));
                } catch (RuntimeException e) {
                    student.setFirstName(String.valueOf((row.getCell(0).getNumericCellValue())));
                }

                try {
                    student.setLastName((row.getCell(1).getStringCellValue()));
                } catch (RuntimeException e) {
                    student.setLastName(String.valueOf((row.getCell(1).getNumericCellValue())));
                }

                try {
                    student.setAge(Integer.parseInt((row.getCell(2).getStringCellValue())));
                } catch (RuntimeException e) {
                    student.setAge((int) row.getCell(2).getNumericCellValue());
                }

                try {
                    student.setGroup((row.getCell(3).getStringCellValue()));
                } catch (RuntimeException e) {
                    student.setGroup(String.valueOf((row.getCell(3).getNumericCellValue())));
                }
                if (student.getFirstName() != null && !student.getFirstName().isEmpty())
                    studentList.add(student);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!studentList.isEmpty()) {
            studentService.create(studentList);
        }
        return studentList;

    }
}
