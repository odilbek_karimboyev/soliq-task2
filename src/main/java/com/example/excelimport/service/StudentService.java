package com.example.excelimport.service;

import com.example.excelimport.payload.StudentRequestDto;

import java.util.List;

public interface StudentService {

    void create(List<StudentRequestDto> request);

}
