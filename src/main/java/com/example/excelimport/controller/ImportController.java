package com.example.excelimport.controller;

import com.example.excelimport.service.ExcelService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RequiredArgsConstructor
@RequestMapping("/read")
@RestController
public class ImportController {

    private final ExcelService excelService;

    @PostMapping
    public void readFile(@RequestBody MultipartFile file) {
        excelService.readFile(file);
    }
}
