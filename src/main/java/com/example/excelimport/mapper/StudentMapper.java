package com.example.excelimport.mapper;

import com.example.excelimport.entity.Student;
import com.example.excelimport.payload.StudentRequestDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface StudentMapper {

    Student fromDto(StudentRequestDto request);

    List<Student> fromDtoList(List<StudentRequestDto> request);
}
