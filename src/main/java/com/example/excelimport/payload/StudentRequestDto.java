package com.example.excelimport.payload;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class StudentRequestDto {

    private String firstName;

    private String lastName;

    private int age;

    private String group;
}
